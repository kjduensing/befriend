package com.duensing;

import java.io.IOException;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        //String hello_world_program = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.";
        //String hard_hello_world_program = ">++++++++[-<+++++++++>]<.>>+>-[+]++>++>+++[>[->+++<<+++>]<<]>-----.>->+++..+++.>-.<<+[>[+>+]>>]<--------------.>>.+++.------.--------.>+.>+.";
        String print_alphabet_program = "> ++++ [ - < ++++++ > ] < ++ >> +++++++ [ - < +++++++++ > ] < + < [ - > + . < ]";
        char[] tokenizedProgram = print_alphabet_program.toCharArray();

        int[] memory = new int[30000];
        int dataPointer = 0;
        int programCounter = 0;
        Stack<Integer> loopStack = new Stack<>();
        boolean skip = false;

        while (programCounter < tokenizedProgram.length) {
            switch (tokenizedProgram[programCounter]) {
                case '+':
                    if (!skip) {
                        memory[dataPointer]++;
                    }
                    break;
                case '-':
                    if (!skip) {
                        memory[dataPointer]--;
                    }
                    break;
                case '>':
                    if (!skip) {
                        dataPointer++;
                    }
                    break;
                case '<':
                    if (!skip) {
                        dataPointer--;
                    }
                    break;
                case '.':
                    if (!skip) {
                        System.out.print((char)memory[dataPointer]);
                    }
                    break;
                case ',':
                    if (!skip) {
                        try {
                            memory[dataPointer] = System.in.read();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case '[':
                    if (!skip) {
                        if (memory[dataPointer] == 0) {
                            skip = true;
                        } else {
                            loopStack.push(programCounter);
                        }
                    }
                    break;
                case ']':
                    if (!skip) {
                        if (memory[dataPointer] != 0) {
                            programCounter = loopStack.peek();
                        } else {
                            loopStack.pop();
                        }
                    }
                    break;
                default:
                    break;
            }

            programCounter++;
        }
    }
}
